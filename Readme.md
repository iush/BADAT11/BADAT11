# Centralized data storage for inventory and server behavior.

> Note: This project was canceled, becose it is more a NoSql Database problem.
> Check : https://gitlab.com/iush/mindat11/logs-sistema

## 1. Definition and scope of the project

### a. Definition

Make **diagnoses and projections** for server farms, require data analysis and processing of large amount of data, these processes can be not very efficient and usually do not have any support.

The administration of servers usually resort to diverse and **varied sources of data**, therefore the handling of the data tends to be inefficient, not totally reliable, nothing homogenous and with frequencies with integrity problems.

It is looking to implement a solution that includes the **centralization of the data**, making use of the relational paradigm and with a database that allows the loading and processing of data in a systematic manner.

The principal sources for the system are now for the analisys, **nmon files and HCM resume** configuration from the HCM scanner.

![rack](images/rack_empty.jpg)

### b. Justification

For the management of computer resources, the **ability to detect problems** without requiring direct inspection, is a basic quality on the part of the administration.

The detail of the **consumption and configuration of the resources** is obtained from sources such as spreadsheets and CVS files, for which there are unjustified delays in the problem detection process and the solution thereof, which directly affects the processes critics of the company.

Additionally, the data sources and the processes that are used for the analysis, do not always guarantee the due processing of the data and therefore it may be the case to work performance problems with incorrect or **incomplete information**.

Due to all the above, it is necessary a solution that allows the **organization, cataloging, administration, centralization, integrity, security, management and access of the information**.

#### Consequences:

- Problems in the operation due to delays in the processes derived from the use of the technological resource.
- Over-costs by over estimating the requirements of the technological resource.

![slow](images/slow.png)

#### Causes:

The causes of incorrectly using the technological resource may be the lack of effective techniques in the estimation of current consumption.

#### Goals:

Estimate and optimize the use of technological resources:

- Centralize logs of different types in a single tool that allows their analysis in a unified and immediate way.
- Automatically detect anomalies in the operation.
- Make predictions about the growth of the platform.
- Develop analysis techniques to optimize the use of the technological resource.

## 2. Proposed System

### a. Panorama.

Performance analysis based on logs, in summary the following objectives are sought.

NMON (Nigel’s Monitor) is written and maintained by Nigel Griffiths from the IBM Technical Sales Support team in the UK. NMON automatically collects a wide range of data, including CPU utilization, memory usage, paging activity, I/O activity, and much more. NMON versions are available for AIX as well as Linux® and can be used to analyze system level performance in System p and System x environments.

### b. Functional requirements.

- The system allow a script to upload the recollected data automatically onto the database. (REST interface.)
- Upload the collected data into a database, and store it into the correct category, regardless of the source host, operating system and / or version.
- Perform data analysis by groupings of several nmon files. (the traditional merge)
- Detect automatically issues on the upladed data.
- Generate a report with the automatically detected issues.
- Facilitate the detection of changes in the configuration. (CPUs, memory real,memory virtual, storage.)
- Allow the saving of these recommendations, which allow the continuity of the task.

#### Identificación de variables de análisis

In particular, we will look for the relationship that the consumption of resources has with the server processes, seeking to infer user response times, number of users and long-term tasks on the servers.

Ver cómo responden a los cambios las diferentes configuraciones, buscando entender el comportamiento de granja de servidores en cuanto a una serie de factores, incluidos:

- Cuántos usuarios simultáneos usan el sistema.
- Qué tipos de operaciones de usuario se realizan.
- Que tipo de procesos se ejecutan en el servidor

#### Tipo de análisis y la técnica de minería de datos que pueda aplicar

Principalmente buscaremos hacer análisis de correlaciones para determinar como se relacionan los recursos entre ellos y luego como afectan a los procesos y en general varias vistas del comportamiento de la misma.

![false_cor](images/correlations.jpeg)

Software involucrado
Conjunto de software involucrado en el desarrollo:

### c. Non Functional requirements.

- The system must have security management.

## 3. System models:

### a. Conceptual design of the database.

![ER Diagram](images/ER_diagram.png)

### c. Logical design of the database.

### d. Appendixes.

#### Data Dictionary:

- The AAA Worksheet
Provides high level information about the AIX version/release levels and server hardware configuration.

- The BBBB Worksheet
Provides a high level summary of the hdisks attached to the server LPAR

```csv
BBBB,0000,name,size(GB),disc attach type
```

```bash
Name size(GB) disc attach type
hdisk3 36.4 SCSI
hdisk2 146.8 SCSI
hdisk7 36.4 SCSI
hdisk6 146.8 SCSI
hdisk0 36.4 SCSI
hdisk1 36.4 SCSI
hdisk4 36.4 SCSI
hdisk5 36.4 SCSI
hdisk624 unknown Hitachi-HDS
hdisk625 unknown Hitachi-HDS
hdisk626 unknown Hitachi-HDS
hdisk627 unknown Hitachi-HDS
hdisk628 unknown Hitachi-HDS 
```

- The BBBC Worksheet
Provides a high level summary of the hdisk to Logical Volume (LV) to file system mapping.


## 4. Documentation.

## 5. Bibliography. 

- http://www-01.ibm.com/support/docview.wss?uid=tss1wp101720&aid=1
